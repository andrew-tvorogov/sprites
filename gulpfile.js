'use strict';
//плагины
const gulp = require('gulp'),
    sass = require('gulp-sass'),
    plumber = require('gulp-plumber'),
    rimraf = require('rimraf'),
    //prefixer = require('gulp-prefixer'),
    //stripCssComments = require('gulp-strip-css-comments'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload;

//пути и настройки
const path = {
    build:{
        html:'build/',
        scss:'build/css/',
        img:'build/img/'
    },
    src:{
        html:'src/index.html',
        scss:'src/scss/main.scss',
        img:'src/img/*.{jpg,png}'
    },
    watch:{
        scss:'src/scss/*.scss',
        html:'src/*.{html,htm}',
        img:'src/img/*.{jpg,gif,png,svg,jpeg}'
    },
    clean:'build/'
};

// configuration
const config = {
    server:{
        baseDir:'./build',
        index:'index.html'
    },
    host:'localhost',
    port:'9999',
    tunnel:true,
    logPrefix: "Sprites"
}

// webserver
gulp.task('webserver', function (done) {
    browserSync(config);
    done();
});

// чистим авгиевы конюшни
gulp.task('clean',function (done) {
    rimraf(path.clean,done);
});

// делаем css из scss
gulp.task('build:scss',function (done) {
    gulp.src(path.src.scss)
        .pipe(plumber())
        .pipe(sass({
            outputStyle:"expanded",//nested,compact,expanded,compressed
            sourcemaps:false
        }))
        /*
        .pipe(prefixer({
            cascade:false,
            remove:true
        }))
        .pipe(stripCssComments({
            preserve:false
        })) */
        .pipe(gulp.dest(path.build.scss))
        .pipe(reload({stream:true}));
    done();
});

// копируем img в build
gulp.task('mv:img', function (done) {
    gulp.src(path.src.img)
        .pipe(gulp.dest(path.build.img))
        .pipe(reload({stream:true}));
    done();
});

// копируем html в build
gulp.task('mv:html', function (done) {
    gulp.src(path.src.html)
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream:true}));
    done();
});

gulp.task('watch', function (done) {
    gulp.watch(path.watch.html, gulp.series('mv:html'),reload({stream:true}));
    gulp.watch(path.watch.img, gulp.series('mv:img'),reload({stream:true}));
    gulp.watch(path.watch.scss, gulp.series('build:scss'));
    gulp.watch(path.build.scss, reload({stream:true}));
    done();
});

gulp.task('default',gulp.series('clean',gulp.parallel('mv:img','mv:html','build:scss'),'webserver','watch'));